public class ex01 {

    public void start() {
        String inputHash = "6XZfhbfU8Il1CSHnO9NimnYMH+emHWLz1p08T+2T/8ZlXmhN6uCATkckXvdR8eBE9MlEQ4TfCDJ/ydjODM6c2g==";
        byte[] targetHash = Passwords.base64Decode(inputHash);

        String[] availableChars = " abcdefghijklmnopqrstuvwxyz0123456789".split("");

        long initial = System.currentTimeMillis();

        for (String fourth : availableChars) {
            for (String third : availableChars) {
                for (String second : availableChars) {
                    for (String first : availableChars) {
                        String password = (fourth + third + second + first).trim();

                        if (password.length() == 0) { continue; }

                        if (Passwords.isInsecureHashMatch(password, targetHash)) {
                            System.out.println("Password was: " + password);
                            System.out.printf("Elapsed time: %.2fs", (System.currentTimeMillis() - initial) / 1000.0 );
                            return;
                        }
                    }
                }
            }
        }


        System.out.println("Elapsed time: " + (initial - System.currentTimeMillis()) / 1000 + "s");
    }

    public static void main(String[] args) {
        new ex01().start();
    }
}
