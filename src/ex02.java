public class ex02 {

    public void start() {
        String inputHash ="jUwKgSbr5K0A8lk9Nq7uY2CIlflZmc8h9U5cDCeEEtwKKST6QEg6uMY+Gz11ytUDqmZM5gtwWrekaaxsQRHtPw==";
        byte[] targetHash=Passwords.base64Decode(inputHash);
        byte[] salt = new byte[1];

        String[] availableChars = " abcdefghijklmnopqrstuvwxyz0123456789".split("");

        long initial = System.currentTimeMillis();

        for(String third: availableChars) {
            for(String second: availableChars) {
                for(String first: availableChars) {
                    for(byte i  = Byte.MIN_VALUE; i < Byte.MAX_VALUE; i++) {
                        //What do I want to do here??
                    }
                        String password = (third + second + first).trim();
                    if (password.length() == 0) { continue; }

                    if (Passwords.isInsecureHashMatch(password, targetHash)) {
                        System.out.println("Password was: " + password);
                        System.out.printf("Elapsed time: %.2fs", (System.currentTimeMillis() - initial) / 1000.0 );
                        return;
                    }
                }
            }

        }
        System.out.println("Elapsed time: " + (initial - System.currentTimeMillis()) / 1000 + "s");
    }


    public static void main(String[] args) {
        new ex02().start();
    }
}
